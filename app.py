import secrets
from flask import Flask, render_template, request, redirect, url_for
from flask_migrate import Migrate
from flask import CORS, cross_origin
from database import db
from forms import PersonaForm
from models import Persona

app = Flask(__name__)
cors = CORS(app)

USER_DB = 'cursos'
PASS_DB = 'Cursos%40Blonder413' # %40 -> @ | %2F -> /
URL_DB = 'localhost'
NAME_DB = 'sap_flask_db'
FULL_URL_DB = f'postgresql://{USER_DB}:{PASS_DB}@{URL_DB}/{NAME_DB}'

app.config['CORS_HEADERS'] = 'ContentType'
app.config['SQLALCHEMY_DATABASE_URI'] = FULL_URL_DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# inicializamos el objeto SQLAlchemy
db.init_app(app)

# configurar flask-migrate
migrate = Migrate()
migrate.init_app(app, db)

# configurar flask-wtf
app.config['SECRET_KEY'] = secrets.token_hex()


@app.route('/')
@app.route('/index')
@app.route('/index.html')
@cross_origin()
def inicio():
    # personas = Persona.query.all()
    # total_personas = len(personas)
    personas = Persona.query.order_by('id')
    total_personas = Persona.query.count()
    app.logger.debug(f'Listado personas: {personas}')
    app.logger.debug(f'Listado personas: {total_personas}')
    return render_template('index.html', personas=personas, total_personas=total_personas)


@app.route('/paginas/<int:page>')
def paginas(page):
    page = db.paginate(db.select(Persona).order_by('id'), per_page=2)
    return render_template("paginas.html", page=page)

@app.route('/ver/<int:id>')
def ver(id):
    # persona = Persona.query.get(id)
    persona = Persona.query.get_or_404(id)
    app.logger.debug(f'persona: {persona}')
    return render_template('ver.html', persona=persona)


@app.route('/crear', methods=['GET', 'POST'])
def crear():
    persona = Persona()
    persona_form = PersonaForm(obj=persona)
    if request.method == 'POST':
        if persona_form.validate_on_submit():
            persona_form.populate_obj(persona)
            app.logger.debug(f'Persona a insertar: {persona}')
            # Insertar el registro
            db.session.add(persona)
            db.session.commit()
            return redirect(url_for('inicio'))
    else:
        return render_template('crear.html', form=persona_form)


@app.route('/editar/<int:id>', methods=['GET', 'POST'])
def editar(id):
    persona = Persona.query.get_or_404(id)
    persona_form = PersonaForm(obj=persona)
    if request.method == 'POST':
        if persona_form.validate_on_submit():
            persona_form.populate_obj(persona)
            app.logger.info(f'Persona a actualizar: {persona}')
            db.session.commit() # el commit es suficiente para guardar la información
            return redirect(url_for('inicio'))
    else:
        return render_template('editar.html', form=persona_form, persona=persona)


@app.route('/eliminar/<int:id>')
def eliminar(id):
    persona = Persona.query.get_or_404(id)
    db.session.delete(persona)
    db.session.commit()
    return redirect(url_for('inicio'))
