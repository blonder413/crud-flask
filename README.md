# Requisitos
```
pip install flask
pip install flask-sqlalchemy
pip install flask-migrate
pip install psycopg2
sudo apt install python3-psycopg2
pip install psycopg2-binary
```

# crear la carpeta de migraciones
```
flask db init
```

Creamos el archivo py con el código necesario para generar el SQL
```
flask db migrate
```

Creamos la tabla en la base de datos
```
flask db upgrade
```

# Cambios en el modelo
Si hacemos cambios en el modelo debemos ejecutar stamp head
```
flask db stamp head
flask db upgrade
```

# Formularios
```
pip install flask-wtf
```